import { addDoc, collection, doc, documentId, getDocs, getFirestore, query, updateDoc, where, writeBatch } from "firebase/firestore"
import { useCartContext } from "../../context/CartContext"

const CartContainer = () => {
    const { cartList, vaciarCarrito, eliminarProducto, precioTotal } = useCartContext()

    const generarOrden = async () => {

        // genrando el objeto
        const order = {}
        order.buyer = {name: 'fede', phone: '123456789', email: 'f@gmail.com'}

        order.items = cartList.map(product => {
            return {
                id: product.id,
                name: product.name,
                price: product.price
            }
        })
         
        order.total = precioTotal()
        
        const db = getFirestore()
        const queryOrders = collection(db, 'orders')
        addDoc(queryOrders, order)
        .then(resp => console.log(resp.id))
        .catch(err => console.log(err))
        // .finally(()=> vaciarCarrito())

        // actulaizar producto de la base de datos
        // const queryActualizar = doc(db, 'items', '39GvW2XccCLl3zrl5sdv')
        // updateDoc(queryActualizar, {
        //     stock: 90
        // })
        // .then(() => console.log('producto actualizado'))

        
        // actualizar el stock
        const queryCollectionStock = collection(db, 'items')

        const queryActulizarStock = query(
            queryCollectionStock, //                   ['jlksjfdgl','asljdfks'] -> ejemplo del map ,  
            where( documentId() , 'in', cartList.map(prod => prod.id) ) // in es que estén en ..         
        )

        const batch = writeBatch(db)

        await getDocs(queryActulizarStock)
        .then(resp => console.log(resp))
        .then(resp => resp.docs.forEach(res => batch.update(res.ref, {
            stock: res.data().stock - cartList.find(item => item.id === res.id).cantidad
        }) ))
        .catch(err => console.log(err))
        .finally(()=> vaciarCarrito())

        batch.commit()

    }

    return (
        <div className="row mt-5" >
            <div className="col mt-5">
                <ul>
                    { cartList.map(producto =>  <li>
                                                    <img src={producto.foto} alt="foto" />                            
                                                    {producto.name} {producto.price} {producto.cantidad}
                                                    <button onClick={() => eliminarProducto(producto.id)}> X </button>
                                                </li> )}
                </ul>
                <button onClick={vaciarCarrito}>Vaciar carrito</button>
            </div>   

            
            <div className="col mt-5">                
                <form className="border border-2 border-primary rounded shadow-lg w-75 p-3" style={{margin: 'auto'}}  >
                    <div className="form-group">
                        <label htmlFor="">Nombre</label>
                        <input 
                            type="text" 
                            className="form-control" 
                            name="name" 
                            placeholder="Nombre" 
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="">Teléfono</label>
                        <input 
                            type="text" 
                            className="form-control" 
                            name="phone" 
                            placeholder="Teléfono" 
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="">Email</label>
                        <input 
                            type="text"
                            className="form-control" 
                            name="email" 
                            placeholder="Email" 
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="">Validar Email</label>
                        <input 
                            type="text"
                            className="form-control" 
                            name="validarEmail" 
                            placeholder="Repita Email" 
                        />
                    </div>
                    <button type="button" className="btn btn-primary mt-2" onClick={generarOrden}>Generar orden</button>
                </form>
            </div>
            {/* <button onClick={generarOrden}>Generar Orden</button> */}
        </div>
    )
}

export default CartContainer













 // actualizar el stock
//  const queryCollectionStock = collection(db, 'items')

//  const queryActulizarStock = query(
//      queryCollectionStock, //                   ['jlksjfdgl','asljdfks'] -> ejemplo del map ,  
//      where( documentId() , 'in', cart.map(it => it.id) ) // in es que estén en ..         
//  )

//  const batch = writeBatch(db)

//  await getDocs(queryActulizarStock)
//  .then(resp => resp.docs.forEach(res => batch.update(res.ref, {
//      stock: res.data().stock - cart.find(item => item.id === res.id).cantidad
//  }) ))
//  .catch(err => console.log(err))
//  .finally(()=> emptyCart())

//  batch.commit()
