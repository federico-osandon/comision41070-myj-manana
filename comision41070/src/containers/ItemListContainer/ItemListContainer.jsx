import { useEffect, useState } from "react"
import { getFetch } from "../../helpers/getFetch"
import { Link, useParams } from 'react-router-dom'
import ItemList from "../../components/ItemList/ItemList"
import Loading from "../../components/Loading/Loading"

import { collection, doc, getDoc, getDocs, getFirestore, limit, orderBy, query, where } from 'firebase/firestore'



const ItemListContainer = () => {       
    const [products, setProducts] = useState([])
    const [product, setProduct] = useState({})
    const [loading, setLoading] = useState(true)
    const [bool, setBool] = useState(true)
    const { categoriaId } = useParams()

    
    // traer todos los productos
    // useEffect(() => {
    //     const db = getFirestore()
    //     const queryColleccion = collection(db, 'items')
    //     getDocs(queryColleccion)
    //     .then(resp => setProducts(resp.docs.map(prod => ({ id: prod.id, ...prod.data()}) )))
    //     .catch(err => console.log(err))
    //     .finally(() => setLoading(false))
    // },[])

    // traer todos los productos FILTRADOS POR CATEGORíA
    useEffect(() => {
        const db = getFirestore()
        const queryColleccion = collection(db, 'items')
        const queryColleccionFiltrado = query(
            queryColleccion, 
            // where('categoria','==', 'remeras')
            where('price','>=', 1000),
            limit(2),
            orderBy('price', 'asc')
        )
        getDocs(queryColleccionFiltrado)
        .then(resp => setProducts(resp.docs.map(prod => ({ id: prod.id, ...prod.data()}) )))
        .catch(err => console.log(err))
        .finally(() => setLoading(false))
    },[])

    console.log(products)
    // useEffect(()=>{
    //     if (categoriaId) {            
    //         getFetch()
    //         .then(resp => setProducts(resp.filter(product => product.categoria === categoriaId)))
    //         .catch( err => console.log(err)) 
    //         .finally(() => setLoading(false))             
    //     } else {
    //         getFetch()
    //         .then(resp => setProducts(resp)) 
    //         .catch( err => console.log(err))
    //         .finally(() => setLoading(false))                   
    //     }
        
    // },[categoriaId])


    //ejemplo de evento
   const handleClick=(e)=>{
        e.preventDefault() 
        setBool(!bool)
    }

    const handleAgregar=()=>{
        setProducts([
            ...products,
            { id: products.length + 1, name: "Gorra 7", url: 'https://www.remerasya.com/pub/media/catalog/product/cache/e4d64343b1bc593f1c5348fe05efa4a6/r/e/remera_negra_lisa.jpg', categoria: "remera" , price: 2 }
        ])
    }

    
    console.log('ItemListContainer')
    
    return (
        <>
            <button onClick={handleClick}>Cambiar estado </button>           
            <button onClick={handleAgregar}>Agregar Item </button>
            {loading ? 
                    <Loading />
                :
                    <ItemList products={products} />        }
        </>
    )
}

export default ItemListContainer