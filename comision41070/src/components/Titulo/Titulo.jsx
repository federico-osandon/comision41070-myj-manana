
const Titulo = ( { titulo, subTitulo } ) => {
   
    return (
        <> 
            <h2> { titulo } </h2>
            <h5>{ subTitulo } </h5>
        </>
    )
}


export default Titulo