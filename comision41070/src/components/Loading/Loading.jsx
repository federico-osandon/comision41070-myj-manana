import { useEffect } from "react"


const Loading = () => {
    // estados
    // useEffect(()=>{
    //     console.log('Loading')
    //     return () => console.log('Desmontando loading')
    // })

    // Componentes hijos
    return (
        <h1>Loading ...</h1>
    )
}

export default Loading