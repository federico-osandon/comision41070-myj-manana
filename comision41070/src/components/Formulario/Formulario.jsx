import Titulo from "../Titulo/Titulo"

const Formulario = ( ) => {
    const titulo = 'Soy titulo de input'
    let subTitulo = 'Soy subtitulo de input'
    return (
        <>
            <Titulo titulo={titulo} subTitulo={subTitulo} />
            <input type="text" placeholder="Nombre" />
            {/* < button onClick={ props.saludar }> Click</button> */}
        </>
    )
}

export default Formulario