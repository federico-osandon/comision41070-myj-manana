import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
import { initFirebase } from './firebase/config'
// import { BrowserRouter } from 'react-router-dom'
import './index.css'


// App() -> <App /> llamada a función en jsx
initFirebase()
ReactDOM.createRoot(document.getElementById('root')).render(<App />)
