import { useEffect } from "react"
import { useParams } from "react-router-dom"

import ItemDetail from "../../components/ItemDetail/ItemDetail"
import { getFetch } from "../../helpers/getFetch"
import { useState } from "react"
import { TextComponent, TextComponent2, TextComponent3, TextComponent4, TextComponent5, TextComponent6, TextComponent7 } from "../../clases/clase11/ComponenteEjemplosCondicionales"
import { doc, getDoc, getFirestore } from "firebase/firestore"

const ItemDetailConteiner = () => {
    const [ producto, setProducto ] =  useState({})
    const { productId } = useParams()

    // traer un producto por id
    useEffect(() => {
        const db = getFirestore()
        const queryProduct  =  doc(db, 'items', productId)
        getDoc(queryProduct)
        .then(resp =>  setProducto( { id: resp.id, ...resp.data() } ))
    }, [productId])

    
    return ( 
        <div>    
            {
                producto.id ?
                    <ItemDetail producto={producto} />
                :
                    <h1>Cargando...</h1>
            }        
            {/* <ItemDetail producto={producto} />             */}
        </div>
    )
}

export default ItemDetailConteiner