// import {  } from 'react';
import { createContext, useState, useContext } from 'react';

const CartContext = createContext([])

export const useCartContext = () => useContext(CartContext)


const CartContexProvider = ({ children }) => {
    // estados y funciones acá
    const [cartList, setCartList] = useState([])

    const agregarCarrito = (objProducto) =>{
        setCartList([ 
            ...cartList,
            objProducto
         ])
    }

    const vaciarCarrito = () =>{
        setCartList([])
    }


    // función total del precio
    const precioTotal = ()=>{
        return cartList.reduce((acumPrecio, prodObj) => acumPrecio = acumPrecio + (prodObj.price * prodObj.cantidad) , 0) // <- precioTotal
    }

    const cantidadTotal = ()=>{
        return cartList.reduce((contador, produObject) => contador += produObject.cantidad , 0) 
    }

    // función que elimina un producto del carrito
    const eliminarProducto = (id) => {
        setCartList( cartList.filter(prod => prod.id !== id ) )
    }
    
   
    

    return (
        <CartContext.Provider value={{
            cartList,
            agregarCarrito,
            vaciarCarrito,
            precioTotal,
            cantidadTotal,
            eliminarProducto
        }}>
            { children }
        </CartContext.Provider>
    )
}

export default CartContexProvider

